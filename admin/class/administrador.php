<?php
class Administrador
{
 //Atributos 
 private $id;
 private $nome;
 private $email;
 private $login;
 private $senha;

 //propriedades = metodos de acesso (Getters and Setters) 

 public function getId(){
  return $this->id;  
 }
 
 public function setId($value){
     $this->id = $value;
 }
 public function getNome(){
    return $this->nome;  
   }
   
   public function setNome($value){
       $this->nome = $value;
   }

   public function getEmail(){
    return $this->email;  
   }
   
   public function setEmail($value){
       $this->email = $value;
   }
   public function getLogin(){
    return $this->login;  
   }
   
   public function setLogin($value){
       $this->login = $value;
   }

   public function getSenha(){
    return $this->senha;  
   }
   
   public function setSenha($value){
       $this->senha = $value;
   }

    //carregar por ID
   public function loadById($_id){       //Implementar ("Use Case")
     $sql = new Sql();
     $results = $sql->select("SELECT * from administrador where id = :id",array(' :id'=>$_id));
        if(count($results)>0){
         $this->seData($results[0]);
       }
    } 
   
    //Gerar uma lista
    public static function getList(){
    $sql = new Sql();
    return $sql->select("SELECT * FROM administrador order by nome");
    }

    public static function search($nome_adm){
     $sql = new Sql();
     return $sql->select("SELECT * FROM administrador where nome LIKE :nome",array(":nome"=>"%".$nome_adm."%"));   //Concatenar = somar texto ...  .-. 
    }
    
    public function efetuarLogin($_login, $_senha){
     $sql = new SQL();
     $senha_cript = md5($_senha);
     $results = $sql->select("SELECT * from administrador where login = :login  and  senha = :senha", array(':login'=>$_login,":senha"=>$senha_cript));
     if(count($results)>0)
     $this->setData($results[0]);

    }

    public function setData($data){
        $this->setId($data['id']);
        $this->setNome($data['nome']);
        $this->setEmail($data['email']);
        $this->setLogin($data['login']);
        $this->setSenha($data['senha']);
    }


    //inserir informações 
    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_adm_insert(:nome, :email, :login, :senha)", 
        array(
            ":nome"=>$this->getNome(),
            "email"=>$this->getEmail(),
            ":login"=>$this->getLogin(), 
            ":senha"=>md5($this->getSenha())

        ));
        if(count($results)>0){
            $this->setData($results[0]);
        }


    }

    public function update($_id, $_nome,$_email,$_senha){
        $sql = new Sql(); 
        $sql->query("UPDATE administrador SET nome = :nome, email = :email, senha = :senha WHERE id = :id", 
        array(
            ":id"=>$this->$_id,
            ":nome"=>$this->$_nome,
            "email"=>$this->$_email(),
            ":senha"=>$this->md5($_senha)

        ));
        
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM administrador WHERE id = :id", array(":id"=>$this->getId()));
    }

    public function __construct($_nome="",$_email="",$_login="",$_senha=""){
        $this->nome = $_nome;
        $this->email = $_email;
        $this->li =$_senha;

    }
    
    
     
 

    
    
     
 
    
    
     
 

 

}
 

?>