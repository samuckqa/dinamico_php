<?php
class Categoria
{
  //Atributos 
  private $id;
  private $categoria;
  private $cat_atvivo;

  //Propriedades = metodos de acesso (Getters e Setter)
/*_____________ID_________________________________*/
 public function getId(){
      return $this->id;
  }
 
  public function setId($value){
      $this->id =$value;
      
  }
/*___________categoria ___________________________________*/
  public function getCategoria(){
    return $this->categoria;
  } 
  
  public function setCategoria($value){
      $this->categoria =$value;
  }
  /*______________categoria_ativa ou não________________________________*/
  public function getCat_ativo(){
    return $this->cat_atvivo;
  } 
  
  public function setCat_ativo($value){
      $this->cat_atvivo =$value;
  }
    
  //metodos construtores 

    //Carregar Por Id
   public function loadById($_id){
    $sql = new Sql();
    $results = $sql->select("SELECT * from categoria where id =:id", array(' :id'=>$_id));
    if(count($results)>0){
      $this->seData($results[0]);
    }


   }
   //Gerar uma lista
   public function getList(){
     $sql = new Sql();
     return $sql->select("SELECT * from categoria");
   }

   //procurar por nome 

   public function search ($nome_cat){
     $sql = new Sql();
     return $sql ->select("SELECT * from categoria where categoria like :nome", array(":nome"=>"%".$nome_cat."%"));

   }
   //
   public function setData($data){
     $this->setId($data['id_categoria']);
     $this->setCategoria($data['categoria']);
     $this->setCat_ativo($data['cat_ativo']);
   }

   public function insert(){
     $sql = new Sql();
     $results = $sql->select("CALL sp_cat_insert(:categoria, :cat_ativo)",
     array(
       ":categoria"=>$this->getCategoria(),
       ":cat_ativo"=>$this->getCat_ativo())
      );
      if (count($results)>0){
        $this->setData($results[0]);
      }
    } 

    public function update($_id, $_categoria, $_cat_ativo){
      $sql = new Sql();
      $sql->query("UPDATE categoria set nome = :categoria, ativo = :cat_ativo where id = :id",
      array(
        ":id_categoria"=>$this->$_id, 
        ":categoria"=>$this->$_categoria, 
        ":cat_ativo"=>$this->$_cat_ativo)

        
      );

    }
    public function delete(){
      $sql =  new Sql();
      $sql->query("DELETE from categoria where id = :id", array(":id"=>$this->getId()));
    }
      
    public function __construct($_categoria="",$_cat_ativo=""){
      $this->categoria = $_categoria;
      $this->cat_atvivo = $_cat_ativo;

    }
    
      
    

  
}

 


?>