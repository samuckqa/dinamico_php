<?php
    class Post{
        // Atributos
        private $id_post;
        private $id_categoria;
        private $titulo_post;
        private $descricao_post;
        private $img_post;
        private $visitas;
        private $data_post;
        private $post_ativo;
        
        // Métodos de acesso (Getters and Setters)
        public function getIdPost(){
            return $this->id_post;
        }
        public function setIdPost($value){
            $this->id_post = $value;
        }
        public function getIdCat(){
        return $this->id_categoria;
        }
        public function setIdCat($value){
            $this->id_categoria = $value;
        }
        public function getTitulo(){
            return $this->titulo_post;
        }
        public function setTitulo($value){
            $this->titulo_post = $value;
        }
        public function getDescricao(){
            return $this->descricao_post;
        }
        public function setDescricao($value){
            $this->descricao_post = $value;
        }
        public function getImg(){
            return $this->img_post;
        }
        public function setImg($value){
            $this->img_post = $value;
        }
        public function getVisita(){
            return $this->visitas;
        }
        public function setVisita($value){
            $this->visitas = $value;
        }
        public function getDataPost(){
            return $this->data_post;
        }
        public function setDataPost($value){
            $this->data_post = $value;
        }
        public function getArtivo(){
            return $this->post_ativo;
        }
        public function setAtivo($value){
            $this->post_ativo = $value;
        }



        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select('SELECT * FROM post WHERE id_post = :id', array(':id'=>$_id));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select('SELECT * FROM post order by titulo_post');
        }
        public static function search($titulo_post){
            $sql = new Sql();
            return $sql->select('SELECT * FROM post WHERE titulo_post LIKE :tiulo', array(':titulo'=>'%'.$titulo_post.'%'));
        }
        public function setData($data){
            $this->setIdPost($data['id_post']);
            $this->setIdCat($data['id_categora']);
            $this->setTitulo($data['titulo_post']);
            $this->setDescricao($data['descricao_post']);
            $this->setImg($data['img_post']);
            $this->setVisita($data['visitas']);
            $this->setDataPost($data['data_post']);
            $this->setAtivo($data['post_ativo']);
        }
        public function insert(){
            $sql = new Sql();
            $results = $sql->select('CALL sp_post_insert(:categoria,:titulo,:descricao,:img,:visita,:data,:ativo', array(
                ':categoria'=>$this->getIdCat(),
                ':titulo'=>$this->getTitulo(),
                ':descricao'=>$this->getDescricao(),
                'img'=>$this->getImg(),
                ':vista'=>$this->getVisita(),
                ':data'=>$this->getDataPost(),
                ':ativo'=>$this->getAtivo()
            ));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        public function update($_id, $_titulo, $_descricao, $_img, $_visita, $_dataPost, $_ativo){
            $sql = new Sql();
            $sql->query('UPDATE post SET titulo_post=:titulo, descricao_post=:descricao, img_post=:img, visitas=:visita, data_post=:data, ativo=:ativo WHERE id_post=:id', array(
                ':id'=>$_id,
                ':titulo'=>$_titulo,
                ':descricao'=>$_descricao,
                ':img'=>$_img,
                ':visita'=>$_visita,
                ':data'=>$_dataPost,
                ':ativo'=>$_ativo
            ));
        }
        public function delete(){
            $sql = new Sql();
            $sql->query('DELETE FROM post WHERE id_post=:id', array(':id'=>$this->getIdPost()));
        }
        public function __construct($_post = '', $_categoria='', $_titulo = '', $_descricao = '', $_img = '', $_visita = '', $_dataPost = '', $_ativo = ''){
            $this->id_post = $_post;
            $this->id_categoria = $_categoria;
            $this->titulo_post = $_titulo;
            $this->descricao_post = $_descricao;
            $this->img_post = $_img;
            $this->visitas = $_visita;
            $this->data_post = $_dataPost;
            $this->post_ativo = $_ativo;
        }
    }
?>