<?php
 class Sql extends PDO{
     private $cn;

     public function __construct(){
         $this->cn = new PDO("mysql:host=127.0.0.1;dbname=dinamico85db","root","");
     }
      
     //metodo para gerar query (metodo atributido para parametros de uma query sql)
     public function setParams($comando, $parametros = array() ){   //paremwetros sera uma matriz de parametros/ usar a matriz para construir a função sql 
        foreach($parametros as $key => $value){
            $this->setParam($comando,$key, $value);
        }
    }

    public function setParam($cmd, $key, $value){    // = (atribuição)| == (comparação) | === (comparação absoluta) | => (associação) | -> (propriedade)
        $cmd->bindParam($key, $value);
    }
    public function query($comandoSql, $params = array()){
        $cmd = $this->cn->prepare($comandoSql);
        $this->setParams($cmd, $params);
        $cmd->execute();
        return $cmd;
    }

     //Função para receber e exibir uma associção de informação de uma matriz gerada por comandoSql 
    public function select($comandoSql, $params = array()){
        $cmd = $this->query($comandoSql,$params);
        return $cmd->fetchAll(PDO::FETCH_ASSOC);
    }


}     
     
     
    
?>