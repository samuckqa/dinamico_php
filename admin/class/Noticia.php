<?php
    class noticia{
        // Atributos
        private $id_noticia;
        private $id_categoria;
        private $titulo_noticia;
        private $img_noticia;
        private $visita_noticia;
        private $data_noticia;
        private $noticia_ativo;
        
        // Métodos de acesso (Getters and Setters)
        public function getIdNoticia(){
            return $this->id_noticia;
        }
        public function setIdNoticia($value){
            $this->id_noticia = $value;
        }
        public function getIdCat(){
        return $this->id_categoria;
        }
        public function setIdCat($value){
            $this->id_categoria = $value;
        }
        public function getTitulo(){
            return $this->titulo_noticia;
        }
        public function setTitulo($value){
            $this->titulo_noticia = $value;
        }
        public function getImg(){
            return $this->img_noticia;
        }
        public function setImg($value){
            $this->img_noticia = $value;
        }
        public function getVisita(){
            return $this->visita_noticia;
        }
        public function setVisita($value){
            $this->visita_noticia = $value;
        }
        public function getDatanoticia(){
            return $this->data_noticia;
        }
        public function setDatanoticia($value){
            $this->data_noticia = $value;
        }
        public function getArtivo(){
            return $this->noticia_ativo;
        }
        public function setAtivo($value){
            $this->noticia_ativo = $value;
        }



        public function loadById($_id){
            $sql = new Sql();
            $results = $sql->select('SELECT * FROM noticia WHERE id_noticia = :id', array(':id'=>$_id));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        public static function getList(){
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticia order by titulo_noticia');
        }
        public static function search($titulo_noticia){
            $sql = new Sql();
            return $sql->select('SELECT * FROM noticia WHERE titulo_noticia LIKE :tiulo', array(':titulo'=>'%'.$titulo_noticia.'%'));
        }
        public function setData($data){
            $this->setIdNoticia($data['id_noticia']);
            $this->setIdCat($data['id_categora']);
            $this->setTitulo($data['titulo_noticia']);
            $this->setImg($data['img_noticia']);
            $this->setVisita($data['visita_noticia']);
            $this->setDatanoticia($data['data_noticia']);
            $this->setAtivo($data['noticia_ativo']);
        }
        public function insert(){
            $sql = new Sql();
            $results = $sql->select('CALL sp_noticia_insert(:categoria,:titulo,:img,:visita,:data,:ativo', array(
                ':categoria'=>$this->getIdCat(),
                ':titulo'=>$this->getTitulo(),
                ':descricao'=>$this->getDescricao(),
                'img'=>$this->getImg(),
                ':vista'=>$this->getVisita(),
                ':data'=>$this->getDatanoticia(),
                ':ativo'=>$this->getAtivo()
            ));
            if (count($results)>0) {
                $this->setData($results[0]);
            }
        }
        public function update($_id, $_titulo, $_img, $_visita, $_datanoticia, $_ativo){
            $sql = new Sql();
            $sql->query('UPDATE noticia SET titulo_noticia=:titulo, img_noticia=:img, visitas=:visita, data_noticia=:data, ativo=:ativo WHERE id_noticia=:id', array(
                ':id'=>$_id,
                ':titulo'=>$_titulo,
                ':img'=>$_img,
                ':visita'=>$_visita,
                ':data'=>$_datanoticia,
                ':ativo'=>$_ativo
            ));
        }
        public function delete(){
            $sql = new Sql();
            $sql->query('DELETE FROM noticia WHERE id_noticia=:id', array(':id'=>$this->getIdNoticia()));
        }
        public function __construct($_noticia = '', $_categoria='', $_titulo = '', $_img = '', $_visita = '', $_datanoticia = '', $_ativo = ''){
            $this->id_noticia = $_noticia;
            $this->id_categoria = $_categoria;
            $this->titulo_noticia = $_titulo;
            $this->img_noticia = $_img;
            $this->visitas = $_visita;
            $this->data_noticia = $_datanoticia;
            $this->noticia_ativo = $_ativo;
        }
    }
?>