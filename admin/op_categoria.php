<?php
if(isset($_POST['txt_categoria'])){
    require_once('conexao.php');
    $categoria = $_POST['txt_categoria'];
    $ativo = isset($_POST['check_ativo'])?'1':'0';
    $cmd = $cn->prepare("sp_cat_insert ");
    $cmd->execute(array(
        ':cat'=>$categoria,
        ':ativ'=>$ativo
    ));
    header('location:principal.php?link=2&msg=ok');
}

function listar_categoria(){
    require_once('conexao.php');
    $query = "select * from categoria";
    $cmd = $cn->prepare($query); //PDO
    $cmd->execute();
    return $cmd->fetchAll(PDO::FETCH_ASSOC);
}

?>0